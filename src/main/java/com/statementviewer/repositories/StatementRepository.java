package com.statementviewer.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.statementviewer.model.Statement;
import java.util.List;


@Repository
public interface StatementRepository extends JpaRepository<Statement, Long> {   
    
	//Since date fields and amount fields are in String format in the DB, filterings are done as service layer logic.
	@Query("SELECT s FROM Statement s INNER JOIN s.account a WHERE a.accountNumber = :accountNumber")
	List<Statement> getStatementsByAccountId(@Param("accountNumber") String accountNumber);

}


