package com.statementviewer.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.statementviewer.model.ErrorResponse;

@RestControllerAdvice
class GlobalControllerExceptionHandler {
	
	//Multiple exceptions from the controller are globally handled here
    @ExceptionHandler({UsernameNotFoundException.class,AccessDeniedException.class, IllegalArgumentException.class})
    public ResponseEntity<ErrorResponse> handleException(Exception ex) {
    	ErrorResponse errorResponse = new ErrorResponse();  
    	errorResponse.setMessage(ex.getMessage());
        
        if (ex instanceof UsernameNotFoundException) {
        	errorResponse.setStatusCode(HttpStatus.FORBIDDEN.value());           
        } else if (ex instanceof AccessDeniedException) {
        	errorResponse.setStatusCode(HttpStatus.UNAUTHORIZED.value());           
        } else if (ex instanceof IllegalArgumentException) {
        	errorResponse.setStatusCode(HttpStatus.BAD_REQUEST.value());            
        } 
        return ResponseEntity.status(errorResponse.getStatusCode()).body(errorResponse);
    
    }
   
}