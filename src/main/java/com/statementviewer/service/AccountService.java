package com.statementviewer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.statementviewer.model.Account;
import com.statementviewer.model.Statement;
import com.statementviewer.repositories.AccountRepository;
import com.statementviewer.repositories.StatementRepository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

@Service
public class AccountService {

	@Autowired
    AccountRepository accountRepository;
    
    @Autowired
    StatementRepository statementRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository, StatementRepository statementRepository) {
        this.accountRepository = accountRepository;
        this.statementRepository = statementRepository;
    }

   
}
