package com.statementviewer.service;

import com.statementviewer.model.Statement;
import java.time.LocalDate;
import java.util.List;

public interface StatementServiceInterface {
	
    List<Statement> getStatementsForThreeMonths(String accountNumber);
    List<Statement> getStatementsByDateRange(List<Statement> statements, LocalDate startDate, LocalDate endDate);
    List<Statement> getStatementsByAmountRange(List<Statement> statements, Double fromAmount, Double toAmount);
    List<Statement> getStatementsForAdminUser(String accountNumber,String fromDate,String toDate,String fromAmount,String toAmount);
}
