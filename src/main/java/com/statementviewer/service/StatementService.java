package com.statementviewer.service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.statementviewer.controller.StatementController;
import com.statementviewer.model.Statement;
import com.statementviewer.repositories.StatementRepository;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StatementService implements StatementServiceInterface {
    private final StatementRepository statementRepository;
    private static final Logger logger = LoggerFactory.getLogger(StatementController.class);    

    public StatementService(StatementRepository statementRepository) {
        this.statementRepository = statementRepository;
    }
    
    public List<Statement> getStatementsForAdminUser(String accountNumber,String fromDateStr,String toDateStr,String fromAmountStr,String toAmountStr) {
    	 
    	 List<Statement> statementsFiltered = null;
    	 DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    	 
		 //If admin user doesn's specify any other parameters other than account number,
    	 //Admin user and user user behave same and retrieves latest 3 months bank statements
    	 if (fromDateStr == null && toDateStr == null && fromAmountStr == null && toAmountStr == null) {	
			 return getStatementsForThreeMonths(accountNumber);             
		 } 		
    	 
    	 //Initially the statements are retrieved based on accountNumber
		 statementsFiltered = statementRepository.getStatementsByAccountId(accountNumber);		 
		 
		 //Filtered statements then undergo the date filtering
		 //If one of the dates is missing, then the other date is calculated 3 months from the available date
		 if (fromDateStr != null && toDateStr != null) {	
			 
			 LocalDate fromDate = LocalDate.parse(fromDateStr, formatter);
			 LocalDate toDate = LocalDate.parse(toDateStr, formatter);	 			 
			 statementsFiltered= getStatementsByDateRange(statementsFiltered,fromDate,toDate);
			 
		 } else if (fromDateStr != null ) {
			 
			 LocalDate fromDate = LocalDate.parse(fromDateStr, formatter);
			 LocalDate toDate = fromDate.plusMonths(3);		
			 logger.info("To date is null. Calculated To Date from 3 months of from date is " + toDate);			 
			 statementsFiltered= getStatementsByDateRange(statementsFiltered,fromDate,toDate);
			 
		 } else if (toDateStr != null) {
			 
			 LocalDate toDate = LocalDate.parse(toDateStr, formatter);
			 LocalDate fromDate = toDate.minusMonths(3);			 
			 logger.info("From date is null. Calculated From Date from 3 months of To date is " + fromDate);			 
			 statementsFiltered= getStatementsByDateRange(statementsFiltered,fromDate,toDate);
		 }		
		 
		 //Finally filtered statements undergo the amount filtering
		 if (fromAmountStr != null && toAmountStr!=null) {
			 
			 double fromAmount = Double.parseDouble(fromAmountStr);
			 double toAmount = Double.parseDouble(toAmountStr);			 
			 statementsFiltered= getStatementsByAmountRange(statementsFiltered,fromAmount,toAmount);
			 
		 } else if (toAmountStr!=null ) {
			 
			 double toAmount = Double.parseDouble(toAmountStr);			 
			 statementsFiltered= getStatementsByAmountRange(statementsFiltered,null,toAmount);
			 
		 } else if (fromAmountStr != null  ) {
			 
			 double fromAmount = Double.parseDouble(fromAmountStr);			 
			 statementsFiltered= getStatementsByAmountRange(statementsFiltered,fromAmount, null);
			 
		 }		 
		 
		 return statementsFiltered; 
    }

    //Functions returns latest bank statements for 3 months
    public List<Statement> getStatementsForThreeMonths(String accountNumber) {
        
    	LocalDate currentDate = LocalDate.now();
        LocalDate threeMonthsBack = currentDate.minusMonths(3);
        
        // Retrieve all statements for the account number
        List<Statement> statements = statementRepository.getStatementsByAccountId(accountNumber);
        
        return getStatementsByDateRange(statements,threeMonthsBack,currentDate);
    }
    
    //The function uses streams and lambda to filter statements based on date range
    public List<Statement > getStatementsByDateRange(List<Statement> statements,LocalDate startDate,LocalDate endDate) {
    	
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        
        return statements.stream()
                .filter(statement -> {
                    LocalDate statementDate = LocalDate.parse(statement.getDatefield(), formatter);
                    return (statementDate.isAfter(startDate) || statementDate.isEqual(startDate)) &&
                            (statementDate.isBefore(endDate) || statementDate.isEqual(endDate));
                })
                .collect(Collectors.toList());
    }
        
    //The function uses streams and lambda to filter statements based on amount range
    public List<Statement > getStatementsByAmountRange(List<Statement> statements,Double fromAmount,Double toAmount) {
    	        
        return statements.stream()
                .filter(statement -> {
                	double statementAmount = Double.parseDouble(statement.getAmount());
                	if (fromAmount == null && toAmount == null) {
                        return true; // Include all statements if both fromAmount and toAmount are null
                    } else if (fromAmount == null) {
                        return statementAmount <= toAmount; // Include statements up to toAmount if fromAmount is null
                    } else if (toAmount == null) {
                        return statementAmount >= fromAmount; // Include statements from fromAmount onwards if toAmount is null
                    } else {
                        return (statementAmount >= fromAmount || Double.compare(statementAmount, fromAmount) == 0)
                                && (statementAmount <= toAmount);
                    }
                })
                .collect(Collectors.toList());
    }       
    
}

