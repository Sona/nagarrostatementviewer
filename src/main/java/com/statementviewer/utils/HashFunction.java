package com.statementviewer.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class HashFunction {

	 private HashFunction() {
	        // Private constructor to hide the implicit public one
	    }

    public static String hashString(String input) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            byte[] hashBytes = messageDigest.digest(input.getBytes());

            StringBuilder hashedString = new StringBuilder();
            for (byte hashByte : hashBytes) {
                String hex = Integer.toHexString(0xff & hashByte);
                if (hex.length() == 1) {
                    hashedString.append('0');
                }
                hashedString.append(hex);
            }
            return hashedString.toString();
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }
}
