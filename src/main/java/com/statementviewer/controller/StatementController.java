package com.statementviewer.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.statementviewer.model.Statement;
import com.statementviewer.service.AccountService;
import com.statementviewer.service.StatementService;
import com.statementviewer.utils.HashFunction;

@Controller
@RequestMapping("/accounts")
public class StatementController {
   
    private final StatementService statementService;   
    private static final Logger logger = LoggerFactory.getLogger(StatementController.class);    
    
    @Autowired
    public StatementController(AccountService accountService, StatementService statementService) {     		
        this.statementService = statementService;
    } 

    @GetMapping("/statements")
    public ResponseEntity<?> getStatements(
			@RequestParam(value = "accountNumber", required = false) String accountNumber, 
            @RequestParam(value = "fromDate", required = false) String fromDate,
            @RequestParam(value = "toDate", required = false) String toDate,
            @RequestParam(value = "fromAmount", required = false) String fromAmount,
            @RequestParam(value = "toAmount", required = false) String toAmount,
            HttpSession httpSession) {
    	
    	//Account number is a mandatory field and it cannot be null
        if (accountNumber != null && !accountNumber.contentEquals("")) {
    	
	    	List<Statement> statements;
	        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	        String username = authentication.getName(); 
	        
	        //To enforce user access levels
	        if (username.equals("admin")) {
	        	
	        	//Validation of parameters is needed only for admin user as user user can only enter accountNumber
	        	validateRequestParameters(fromDate,toDate,fromAmount,toAmount);
	        	
	        	//Following call propagates through the layers to get the statements from the MS Access DB
	        	statements = retrieveStatementsForAdminUser(accountNumber,fromDate,toDate,fromAmount,toAmount);  
	        	
	        	//Once the statements are retrieved, account numbers are to be hashed before sending them to the user
	        	List<Statement> hashedStatements = hashAccountNumbers(statements);
	        	
                return ResponseEntity.ok(hashedStatements);
                
	        } else if (username.equals("user")) {
	        	
	        	//User cannot specify any parameter other than accountNumber
	            if (fromDate == null && toDate == null && fromAmount == null && toAmount == null) {	   
	            	
	            	//Retrieve statements from the DB for the account number specified for the latest 3 months
	                statements = retrieveStatementsForUser(accountNumber);  
	                
	                //Once the statements are retrieved, account numbers are to be hashed before sending them to the user
	                List<Statement> hashedStatements = hashAccountNumbers(statements);
	                
	                return ResponseEntity.ok(hashedStatements);	                
	            } else {	        
	            	logger.warn("Unauthorized access");
	            	throw new AccessDeniedException("Unauthorized access");  	            	
	            }
	        } else {	  
	        	logger.warn("Invalid User");
	        	throw new UsernameNotFoundException("Invalid User");	        	
	        }
        } else {
        	logger.warn("Account number cannot be null");
        	throw new IllegalArgumentException("Account number cannot be null");
        }
    }    
    
    private List<Statement> retrieveStatementsForUser(String accountNumber) {        
    		return statementService.getStatementsForThreeMonths(accountNumber);
    }
    
    private List<Statement> retrieveStatementsForAdminUser(String accountNumber,String fromDate,String toDate,String fromAmount,String toAmount) {       
		return statementService.getStatementsForAdminUser(accountNumber,fromDate,toDate,fromAmount,toAmount);
    }
    
    public void validateRequestParameters(String fromDate,String toDate,String fromAmount,String toAmount) {
    	
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    	
    	// From date entered by user should be parsable to LocalDate value using the DD.MM.YYYY formatter
        LocalDate fromDateValue = null;        
        if (fromDate != null) {
            try {
                fromDateValue = LocalDate.parse(fromDate,formatter);
                logger.info("From date value " + fromDateValue + " is ok");
            } catch (DateTimeParseException ex) {
            	logger.warn("Invalid fromDate format. Expected format: dd.mm.yyyy");
                throw new IllegalArgumentException("Invalid fromDate format. Expected format: dd.mm.yyyy");
            }
        }

        // To date entered by user should be parsable to LocalDate value using the DD.MM.YYYY formatter
        LocalDate toDateValue = null;
        if (toDate != null) {
            try {
                toDateValue = LocalDate.parse(toDate,formatter);
                logger.info("To date value " + toDateValue + " is valid");
            } catch (DateTimeParseException ex) {
            	logger.warn("Invalid toDate format. Expected format: dd.mm.yyyy");
                throw new IllegalArgumentException("Invalid toDate format. Expected format: dd.mm.yyyy");
            }
        }

        // From amount entered by user should be parsable to Double
        Double fromAmountValue = null;
        if (fromAmount != null) {
            try {
                fromAmountValue = Double.parseDouble(fromAmount);
                logger.info("From amount value " + fromAmountValue + " is valid!");
            } catch (NumberFormatException ex) {
            	logger.warn("Invalid fromAmount format. Expected a numeric value.");
                throw new IllegalArgumentException("Invalid fromAmount format. Expected a numeric value.");
            }
        }

        // To amount entered by user should be parsable to Double
        Double toAmountValue = null;
        if (toAmount != null) {
            try {
                toAmountValue = Double.parseDouble(toAmount);
                logger.info("To amount value " + toAmountValue + " is accepted");
            } catch (NumberFormatException ex) {
            	logger.warn("Invalid toAmount format. Expected a numeric value.");
                throw new IllegalArgumentException("Invalid toAmount format. Expected a numeric value.");
            }
        }
    }
    
    //The hash function uses the hasString() function to hash the account number
    //Streams and lambda are used to get each account number from the retrieved statements and then to map the account number
    //to its hashed value. The hashed value is then set as the new account number. 
    //Collector collects the filtered list.
    public List<Statement> hashAccountNumbers(List<Statement> statements) {
        return statements.stream()
                .map(statement -> {
                    String hashedAccountNumber = HashFunction.hashString(statement.getAccount().getAccountNumber());
                    statement.getAccount().setAccountNumber(hashedAccountNumber);
                    return statement;
                })
                .collect(Collectors.toList());
    }
}




