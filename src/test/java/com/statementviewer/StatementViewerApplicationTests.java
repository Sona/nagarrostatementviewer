package com.statementviewer;

import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;


import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@SpringBootTest
class StatementViewerApplicationTests {

    @Test
    void contextLoads() {
        // Verify that the application context loads without any exceptions
        assertDoesNotThrow(() -> SpringApplication.run(StatementViewerApplication.class));
    }
}
