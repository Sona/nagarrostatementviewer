package com.statementviewer.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.statementviewer.model.Account;
import com.statementviewer.model.Statement;
import com.statementviewer.repositories.StatementRepository;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.Mockito.when;

class StatementServiceTest {

    @Mock
    private StatementRepository statementRepository;

    @InjectMocks
    private StatementService statementService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        // Create and persist account data
        Account account1 = new Account();
        account1.setAccountType("current");
        account1.setAccountNumber("0012250016001");

        Account account2 = new Account();
        account2.setAccountType("savings");
        account2.setAccountNumber("0012250016002");

        Account account3 = new Account();
        account3.setAccountType("current");
        account3.setAccountNumber("0012250016003");

        Account account4 = new Account();
        account4.setAccountType("current");
        account4.setAccountNumber("0012250016004");

        Account account5 = new Account();
        account5.setAccountType("savings");
        account5.setAccountNumber("0012250016005");

        // Create and persist statement data
        List<Statement> statements = new ArrayList<>();
        statements.add(createStatement(account3, "09.08.2020", "535.197875027054"));
        statements.add(createStatement(account4, "15.11.2020", "967.410308637791"));
        statements.add(createStatement(account3, "03.09.2020", "623.461804295262"));
        statements.add(createStatement(account3, "03.02.2020", "330.455004587924"));
        statements.add(createStatement(account3, "19.05.2020", "125.51573044332"));
        statements.add(createStatement(account4, "12.03.2020", "256.292396032404"));
        statements.add(createStatement(account3, "15.09.2020", "87.8901139771573"));
        statements.add(createStatement(account4, "22.06.2020", "386.908121686113"));
        statements.add(createStatement(account1, "14.10.2020", "196.801905945903"));
        statements.add(createStatement(account5, "25.03.2020", "997.740129408144"));    

        when(statementRepository.getStatementsByAccountId(ArgumentMatchers.anyString()))
        .thenReturn(statements);
    }

    private Statement createStatement(Account account, String datefield, String amount) {
        Statement statement = new Statement();
        statement.setAccount(account);
        statement.setDatefield(datefield);
        statement.setAmount(amount);
        return statement;
    }

    @Test
    void testGetStatementsForAdminUserOnlyAccountNumber() {
        // Mock input data for admin user with only accountNumber
        String accountNumber = "0012250016003";
        String fromDateStr = null;
        String toDateStr = null;
        String fromAmountStr = null;
        String toAmountStr = null;

        // Call the service method
        List<Statement> result = statementService.getStatementsForAdminUser(
                accountNumber,
                fromDateStr,
                toDateStr,
                fromAmountStr,
                toAmountStr
        );    

        // Assertions
        Assertions.assertNotNull(result);
        
    }
    
    @Test
    void testGetStatementsForAdminUserAllParamPresent() {
        // Mock input data for admin user with only accountNumber
        String accountNumber = "0012250016003";
        String fromDateStr = "01.01.2023";
        String toDateStr = "31.01.2023";
        String fromAmountStr = "100";
        String toAmountStr = "3200";

        // Call the service method
        List<Statement> result = statementService.getStatementsForAdminUser(
                accountNumber,
                fromDateStr,
                toDateStr,
                fromAmountStr,
                toAmountStr
        );
        
        // Assertions
        Assertions.assertNotNull(result);
        
    }
    
    @Test
    void testGetStatementsForAdminUserFromDateFromAmountNull() {
        // Mock input data for admin user with only accountNumber
        String accountNumber = "0012250016003";
        String fromDateStr = null;
        String toDateStr = "31.01.2023";
        String fromAmountStr = null;
        String toAmountStr = "3200";

        // Call the service method
        List<Statement> result = statementService.getStatementsForAdminUser(
                accountNumber,
                fromDateStr,
                toDateStr,
                fromAmountStr,
                toAmountStr
        );
  
        // Assertions
        Assertions.assertNotNull(result);
        
    }
    
    @Test
    void testGetStatementsForAdminUserToDateToAmountNull() {
        // Mock input data for admin user with only accountNumber
        String accountNumber = "0012250016003";
        String fromDateStr = "31.01.2023";
        String toDateStr = null;
        String fromAmountStr = "100";
        String toAmountStr = null;

        // Call the service method
        List<Statement> result = statementService.getStatementsForAdminUser(
                accountNumber,
                fromDateStr,
                toDateStr,
                fromAmountStr,
                toAmountStr
        );
        
        // Assertions
        Assertions.assertNotNull(result);
        
    }
    
    @Test
    void testGetStatementsForAdminUser() {
        // Mock input data for admin user 
        String accountNumber = "0012250016003";
        String fromDateStr = "01.01.2023";
        String toDateStr = "31.01.2023";
        String fromAmountStr = null;
        String toAmountStr = null;

        // Call the service method
        List<Statement> result = statementService.getStatementsForAdminUser(
                accountNumber,
                fromDateStr,
                toDateStr,
                fromAmountStr,
                toAmountStr
        );

        // Assertions
        Assertions.assertNotNull(result);
    }

    @Test
    void testGetStatementsForThreeMonths() {
        // Mock input data
        String accountNumber = "0012250016003";

        // Call the service method
        List<Statement> result = statementService.getStatementsForThreeMonths(accountNumber);

        // Assertions
        Assertions.assertNotNull(result);
    }
    
    @Test
    void testGetStatementsByAmountRangeFromAmountToAmount() {
        // Mock input data
    	Double fromAmount=100.2;
        Double toAmount=2500.0;      
      
        Account account3 = new Account();
        account3.setAccountType("current");
        account3.setAccountNumber("0012250016003");

        Account account4 = new Account();
        account4.setAccountType("current");
        account4.setAccountNumber("0012250016004");
       
        List<Statement> statements = new ArrayList<>();
        statements.add(createStatement(account3, "09.08.2020", "535.197875027054"));
        statements.add(createStatement(account4, "15.11.2020", "967.410308637791"));
        statements.add(createStatement(account3, "03.09.2020", "623.461804295262"));
        statements.add(createStatement(account3, "03.02.2020", "330.455004587924"));
        statements.add(createStatement(account3, "19.05.2020", "125.51573044332"));
        statements.add(createStatement(account4, "12.03.2020", "256.292396032404"));
        statements.add(createStatement(account3, "15.09.2020", "87.8901139771573"));
        statements.add(createStatement(account4, "22.06.2020", "386.908121686113"));

        // Call the service method
        List<Statement> result = statementService.getStatementsByAmountRange(statements,fromAmount,toAmount);
        // Assertions
        Assertions.assertNotNull(result);
    }
    
    @Test
    void testGetStatementsByAmountRangeFromAmountToAmountNull() {
        // Mock input data
    	Double fromAmount=null;
        Double toAmount=null;      
      
        Account account3 = new Account();
        account3.setAccountType("current");
        account3.setAccountNumber("0012250016003");

        Account account4 = new Account();
        account4.setAccountType("current");
        account4.setAccountNumber("0012250016004");
       
        List<Statement> statements = new ArrayList<>();
        statements.add(createStatement(account3, "09.08.2020", "535.197875027054"));
        statements.add(createStatement(account4, "15.11.2020", "967.410308637791"));
        statements.add(createStatement(account3, "03.09.2020", "623.461804295262"));
        statements.add(createStatement(account3, "03.02.2020", "330.455004587924"));
        statements.add(createStatement(account3, "19.05.2020", "125.51573044332"));
        statements.add(createStatement(account4, "12.03.2020", "256.292396032404"));
        statements.add(createStatement(account3, "15.09.2020", "87.8901139771573"));
        statements.add(createStatement(account4, "22.06.2020", "386.908121686113"));

        // Call the service method
        List<Statement> result = statementService.getStatementsByAmountRange(statements,fromAmount,toAmount);
        // Assertions
        Assertions.assertNotNull(result);
    }
    
    @Test
    void testGetStatementsByAmountRangeFromAmountNull() {
        // Mock input data
    	Double fromAmount=null;
        Double toAmount=6000.0;      
      
        Account account3 = new Account();
        account3.setAccountType("current");
        account3.setAccountNumber("0012250016003");

        Account account4 = new Account();
        account4.setAccountType("current");
        account4.setAccountNumber("0012250016004");
       
        List<Statement> statements = new ArrayList<>();
        statements.add(createStatement(account3, "09.08.2020", "535.197875027054"));
        statements.add(createStatement(account4, "15.11.2020", "967.410308637791"));
        statements.add(createStatement(account3, "03.09.2020", "623.461804295262"));
        statements.add(createStatement(account3, "03.02.2020", "330.455004587924"));
        statements.add(createStatement(account3, "19.05.2020", "125.51573044332"));
        statements.add(createStatement(account4, "12.03.2020", "256.292396032404"));
        statements.add(createStatement(account3, "15.09.2020", "87.8901139771573"));
        statements.add(createStatement(account4, "22.06.2020", "386.908121686113"));

        // Call the service method
        List<Statement> result = statementService.getStatementsByAmountRange(statements,fromAmount,toAmount);
        // Assertions
        Assertions.assertNotNull(result);
    }
    
    @Test
    void testGetStatementsByAmountRangeToAmountNull() {
        // Mock input data
    	Double toAmount=null;
        Double fromAmount=6000.0;      
      
        Account account3 = new Account();
        account3.setAccountType("current");
        account3.setAccountNumber("0012250016003");

        Account account4 = new Account();
        account4.setAccountType("current");
        account4.setAccountNumber("0012250016004");
       
        List<Statement> statements = new ArrayList<>();
        statements.add(createStatement(account3, "09.08.2020", "535.197875027054"));
        statements.add(createStatement(account4, "15.11.2020", "967.410308637791"));
        statements.add(createStatement(account3, "03.09.2020", "623.461804295262"));
        statements.add(createStatement(account3, "03.02.2020", "330.455004587924"));
        statements.add(createStatement(account3, "19.05.2020", "125.51573044332"));
        statements.add(createStatement(account4, "12.03.2020", "256.292396032404"));
        statements.add(createStatement(account3, "15.09.2020", "87.8901139771573"));
        statements.add(createStatement(account4, "22.06.2020", "386.908121686113"));

        // Call the service method
        List<Statement> result = statementService.getStatementsByAmountRange(statements,fromAmount,toAmount);
        // Assertions
        Assertions.assertNotNull(result);
    }
}
