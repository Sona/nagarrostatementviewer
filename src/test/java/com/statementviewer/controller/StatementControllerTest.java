package com.statementviewer.controller;


import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import com.statementviewer.model.Account;
import com.statementviewer.model.Statement;
import com.statementviewer.service.AccountService;
import com.statementviewer.service.StatementService;

class StatementControllerTest {
    
    @Mock
    private AccountService accountService;
    
    @Mock
    private StatementService statementService;
    
    @Mock
    private HttpSession httpSession;
    
    @InjectMocks
    private StatementController statementController;
    
    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }
    
    @Test
    void testGetStatementsForAdminUser() {
        String accountNumber = "123456789";
        String fromDate = "01.01.2023";
        String toDate = "31.01.2023";
        String fromAmount = "100.0";
        String toAmount = "1000.0";
        
        List<Statement> statements = new ArrayList<>();
              
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.getName()).thenReturn("admin");
        SecurityContextHolder.setContext(securityContext);
        
        when(statementService.getStatementsForAdminUser(anyString(), anyString(), anyString(), anyString(), anyString()))
                .thenReturn(statements);
        
        ResponseEntity<?> response = statementController.getStatements(accountNumber, fromDate, toDate, fromAmount, toAmount, httpSession);
        
    }
    
    @Test
    void testGetStatementsForUser() {
        String accountNumber = "123456789";
        
        List<Statement> statements = new ArrayList<>();    
        
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.getName()).thenReturn("user");
        SecurityContextHolder.setContext(securityContext);
        
        when(statementService.getStatementsForThreeMonths(anyString())).thenReturn(statements);
        
        ResponseEntity<?> response = statementController.getStatements(accountNumber, null, null, null, null, httpSession);
          
    }
    
    @Test
    void testGetStatementsUnauthorizedAccess() {
        String accountNumber = "123456789";

        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);

        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.getName()).thenReturn("user");
        SecurityContextHolder.setContext(securityContext);

        // Use assertThrows to verify the AccessDeniedException
        assertThrows(AccessDeniedException.class, () -> {
            statementController.getStatements(accountNumber, "01.01.2023", "31.01.2023", null, null, httpSession);
        });
    }
    
    @Test
    void testGetStatementsInvalidUser() {
        String accountNumber = "123456789";

        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);

        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.getName()).thenReturn("invalidUser");
        SecurityContextHolder.setContext(securityContext);

        // Use assertThrows to verify the UsernameNotFoundException
        assertThrows(UsernameNotFoundException.class, () -> {
            statementController.getStatements(accountNumber, null, null, null, null, httpSession);
        });
    }
    
    private Statement createStatement(Account account, String datefield, String amount) {
        Statement statement = new Statement();
        statement.setAccount(account);
        statement.setDatefield(datefield);
        statement.setAmount(amount);
        return statement;
    }    

}
