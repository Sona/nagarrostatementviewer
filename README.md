# Statement Viewer

Statement-Viewer application effectively retrieves bank statements for a given account number. There are 2 users for the application, admin and user. User user can only retrieve statements for 3 months, whereas admin can specify date range and amount range for statement filtering.

## Specifications
1. Programming Language : Java 13.0.1
2. Java Framework: Spring boot 2.7.13 
3. Build tool: Gradle 8.1
4. Database: MS Access ucanaccess-5.0.0
5. Test Frameworks: Jacoco, Mockito
6. Logging Framework: SLF4J
7. IDE: Eclipse 

## Build and Run

1. Download the codebase from Git and import the project as a Gradle project in a any IDE. I have used Eclipse.
2. Once the project is loaded, build and run it. Application should now be listening at localhost:8080.

## Test Run
1. Open Google Chrome web browser and enter the URL http://localhost:8080/. You will now be prompted to login. Use 'admin' user. 
2. Once logged in, enter the below url to run a sample test.
http://localhost:8080/accounts/statements?accountNumber=0012250016001&fromDate=10.10.2020
This will return the below response:
[{"account":{"id":1,"accountType":"current","accountNumber":"043728d935f7aae09de8fe67ea56c63f60213133cea0bdb42ff76891fa4b7597"},"id":9,"datefield":"14.10.2020","amount":"196.801905945903"}]

More test cases and various scenaiors are well covered in the document shared - Statement Viewer Documentation.

## Generate Test Report Using Jacoco and Mockito
1. To build the reports, navigate to project directory where gradlew is present and then run the following command
'gradlew clean test jacocoTestReport'.
2. Reports will be generated at statement-viewer\build\reports\jacoco\test



